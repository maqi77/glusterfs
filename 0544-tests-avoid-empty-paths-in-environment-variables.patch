From 3eaf937e69fe4219738c93d39af1cc909b1ee3f8 Mon Sep 17 00:00:00 2001
From: Rinku Kothiya <rkothiya@redhat.com>
Date: Fri, 23 Apr 2021 09:30:35 +0000
Subject: [PATCH 544/584] tests: avoid empty paths in environment variables

Many variables containing paths in env.rc.in are defined in a way
that leave a trailing ':' in the variable when the previous value
was empty or undefined.

In the particular case of 'LD_PRELOAD_PATH' variable, this causes
that the system looks for dynamic libraries in the current working
directory. When this directory is inside a Gluster mount point, a
significant delay is caused each time a program is run (and testing
framework can run lots of programs for each test).

This patch prevents that variables containing paths could end with
a trailing ':'.

Backport of :
>Upstream-patch-link: https://github.com/gluster/glusterfs/pull/2349
>Fixes: #2348
>Change-Id: I669f5a78e14f176c0a58824ba577330989d84769
>Signed-off-by: Xavi Hernandez <xhernandez@redhat.com>
>Signed-off-by: Rinku Kothiya <rkothiya@redhat.com>

Change-Id: Ie903ca443aa4789553ac4687818a7f69c113af41
Signed-off-by: Rinku Kothiya <rkothiya@redhat.com>
---
 tests/env.rc.in | 17 +++++++----------
 1 file changed, 7 insertions(+), 10 deletions(-)

diff --git a/tests/env.rc.in b/tests/env.rc.in
index 1f0ca88..2d8ff0e 100644
--- a/tests/env.rc.in
+++ b/tests/env.rc.in
@@ -2,34 +2,31 @@ prefix=@prefix@
 exec_prefix=@exec_prefix@
 libdir=@libdir@
 
-PATH=@sbindir@:$PATH
+PATH=@bindir@:@sbindir@${PATH:+:${PATH}}
 export PATH
 
 GLUSTERD_PIDFILEDIR=@localstatedir@/run/gluster
 export GLUSTERD_PIDFILEDIR
 
-LD_LIBRARY_PATH=@libdir@:$LD_LIBRARY_PATH
+LD_LIBRARY_PATH=@libdir@${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
 export LD_LIBRARY_PATH
 
-LIBRARY_PATH=@libdir@:$LIBRARY_PATH
+LIBRARY_PATH=@libdir@${LIBRARY_PATH:+:${LIBRARY_PATH}}
 export LIBRARY_PATH
 
-CPATH=@includedir@:$CPATH
+CPATH=@includedir@${CPATH:+:${CPATH}}
 export CPATH
 
 GLUSTERD_WORKDIR=@GLUSTERD_WORKDIR@
 export GLUSTERD_WORKDIR
 
-PKG_CONFIG_PATH=@pkgconfigdir@:$PKG_CONFIG_PATH
+PKG_CONFIG_PATH=@pkgconfigdir@${PKG_CONFIG_PATH:+:${PKG_CONFIG_PATH}}
 export PKG_CONFIG_PATH
 
-PYTHONPATH=@BUILD_PYTHON_SITE_PACKAGES@:$PYTHON_PATH
-export PYTHONPATH
-
 PYTHON=@PYTHON@
 export PYTHON
 
-PYTHONPATH=@BUILD_PYTHON_SITE_PACKAGES@:$PYTHON_PATH
+PYTHONPATH=@BUILD_PYTHON_SITE_PACKAGES@${PYTHONPATH:+:${PYTHONPATH}}
 export PYTHONPATH
 
 GLUSTER_CMD_DIR=@sbindir@
@@ -42,4 +39,4 @@ RUN_NFS_TESTS=@BUILD_GNFS@
 export RUN_NFS_TESTS
 
 GLUSTER_XLATOR_DIR=@libdir@/glusterfs/@PACKAGE_VERSION@/xlator
-export GLUSTER_XLATOR_DIR
\ No newline at end of file
+export GLUSTER_XLATOR_DIR
-- 
1.8.3.1

